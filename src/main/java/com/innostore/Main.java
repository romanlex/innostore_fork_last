package com.innostore;

import com.innostore.app.Application;
import com.innostore.core.Core;
import com.innostore.core.exceptions.CoreException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.text.ParseException;

/**
 * Entry point on application
 * @author Roman Olin <ceo.roman@gmail.com>
 */

public class Main {
    private static final Logger Log = LoggerFactory.getLogger(Main.class);
    public static void main(String[] arg) throws ParseException, SQLException, IllegalAccessException {
        initCore();
        Application app = new Application();
        app.start();

    }


    /**
     * Initialize com.innostore.core of application
     */
    private static void initCore() {
        try {
            Core core = new Core();
            core.load();
        } catch (CoreException e) {
            Log.error(e.getMessage(), Main.class);
        }
    }
}
