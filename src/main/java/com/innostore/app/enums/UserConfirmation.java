package com.innostore.app.enums;

import java.sql.Timestamp;

public class UserConfirmation {
    private int id;
    private int userId;
    private String code;
    private Timestamp date;
    private String confirmed;

    public UserConfirmation() {
    }

    public UserConfirmation(int id, int userId, String code, Timestamp date, String confirmed) {
        this.id = id;
        this.userId = userId;
        this.code = code;
        this.date = date;
        this.confirmed = confirmed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }
}
