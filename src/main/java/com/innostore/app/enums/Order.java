package com.innostore.app.enums;

import java.util.ArrayList;
import java.util.Date;

public class Order {
    private int id;
    private int userId;
    private int statusId;
    private Date createdon;
    private Date updatedon;
    private String comment;
    private Double cost;
    private ArrayList<Product> products;

    public Order() {

    }

    public Order(int id, int userId, int statusId, Date createdon, Date updatedon, String comment, Double cost, ArrayList<Product> products) {
        this.id = id;
        this.userId = userId;
        this.statusId = statusId;
        this.createdon = createdon;
        this.updatedon = updatedon;
        this.comment = comment;
        this.cost = cost;
        this.products = products;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public Date getCreatedon() {
        return createdon;
    }

    public void setCreatedon(Date createdon) {
        this.createdon = createdon;
    }

    public Date getUpdatedon() {
        return updatedon;
    }

    public void setUpdatedon(Date updatedon) {
        this.updatedon = updatedon;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public ArrayList<Product> getOrderProductsList() {
        return products;
    }

    public void setOrderProductsList(ArrayList<Product> products) {
        this.products = products;
    }

}



