package com.innostore.app.enums;

public class OrderStatus {
    private int id;
    private String name;
    private String description;
    private String email_subject;
    private String email_message;

    public OrderStatus() {

    }

    public OrderStatus(int id, String name, String description, String email_subject, String email_message) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.email_subject = email_subject;
        this.email_message = email_message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getEmail_subject() {
        return email_subject;
    }

    public void setEmail_subject(String email_subject) {
        this.email_subject = email_subject;
    }

    public String getEmail_message() {
        return email_message;
    }

    public void setEmail_message(String email_message) {
        this.email_message = email_message;
    }
}
