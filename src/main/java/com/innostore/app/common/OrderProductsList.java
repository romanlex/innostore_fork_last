package com.innostore.app.common;


import com.innostore.app.models.OrderProductsModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "products")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderProductsList {
    @XmlElement(name = "product")
    private List<OrderProductsModel> orderProducts = null;

    public List<OrderProductsModel> getOrderProducts() {
        return orderProducts;
    }

    public void setOrderProducts(List<OrderProductsModel> orderProducts) {
        this.orderProducts = orderProducts;
    }
}
