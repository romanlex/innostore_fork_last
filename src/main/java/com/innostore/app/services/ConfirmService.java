package com.innostore.app.services;

import com.innostore.app.enums.UserConfirmation;
import com.innostore.core.exceptions.ConfirmDAOException;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;

public interface ConfirmService {
    /**
     * Поиск записи по коду
     * @param code код
     * @return UserConfirmation
     * @throws ConfirmDAOException
     * @throws ModelException
     * @throws CoreException
     */
    UserConfirmation findByCode(String code) throws ConfirmDAOException, ModelException, CoreException;

    /**
     * Обновление/сохранение информации о конфирме
     * @param confirm объект UserConfirmation
     * @return количество затронутых строк в БД
     * @throws ConfirmDAOException ошибка при поиске записи
     */
    long save(UserConfirmation confirm) throws ConfirmDAOException, ModelException;
}
