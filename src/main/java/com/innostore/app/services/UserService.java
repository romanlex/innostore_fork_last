package com.innostore.app.services;

import com.innostore.app.enums.User;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;

public interface UserService {
    /**
     * Проверяет авторизацию пользователя
     * @param login логин пользователя
     * @param password пароль пользователя
     * @return объект юзера
     * @throws UserDAOException ошибка при запросе в БД
     */
    User authenticated(String login, String password) throws UserDAOException;

    /**
     * Проверяет существует ли юзер с данным логином в базе
     * @param login логин юзера
     * @return true/false
     * @throws UserDAOException ошибка при поиске записи
     */
    boolean userExist(String login) throws UserDAOException;

    /**
     * Получает список всех юзеров из базы данных
     * @return список юзеров
     * @throws UserDAOException ошибка при поиске записи
     */
    ArrayList<User> find() throws UserDAOException;

    /**
     * Получает список юзеров по условию condition из базы данных
     * @param condition условия выборки из базы данных
     * @return список юзеров
     * @throws UserDAOException ошибка при поиске записи
     */
    ArrayList<User> find(String condition) throws UserDAOException;

    /**
     * Получает юзера по ID из базы данных
     * @param id ID в БД
     * @return объект юзера
     * @throws UserDAOException ошибка при поиске записи
     */
    User getUserById(int id) throws UserDAOException;

    /**
     * Получает юзера по логину из базы данных
     * @param email логин в БД
     * @return объект юзера
     * @throws UserDAOException ошибка при поиске записи
     */
    User getUserByLogin(String email) throws UserDAOException;

    /**
     * Получает список юзеров по роли из базы данных
     * @param role Роль юзера
     * @return объект юзера
     * @throws UserDAOException ошибка при поиске записи
     */
    ArrayList<User> getUsersByRole(String role) throws UserDAOException;

    /**
     * Удаление юзера из БД
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws UserDAOException ошибка при поиске записи
     */
    int deleteUser(HttpServletRequest req) throws UserDAOException;

    /**
     * Обработка запроса пользователя на сохранение/обновление пользователя в базе
     * @param req HttpServletRequest запрос
     * @return количество затронутых строк в БД
     * @throws UserDAOException ошибка при поиске записи
     */
    long save(HttpServletRequest req) throws UserDAOException, ModelException;

    /**
     * Регистрация пользователя
     * @param email email
     * @param password password
     * @param firstname firstname
     * @return количество затронутых строк в БД
     * @throws UserDAOException ошибка при поиске записи
     * @throws ModelException ошибка в модели
     * @throws CoreException ошибка при генерации хэша
     */
    long join(String email, String password, String firstname) throws UserDAOException, ModelException, CoreException;
}
