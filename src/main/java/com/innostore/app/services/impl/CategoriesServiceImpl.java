package com.innostore.app.services.impl;

import com.innostore.app.enums.Category;
import com.innostore.app.enums.User;
import com.innostore.app.repository.CategoriesRepository;
import com.innostore.app.services.CategoriesService;
import com.innostore.core.exceptions.CategoryDAOException;
import com.innostore.core.exceptions.ModelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

@Service
public class CategoriesServiceImpl implements CategoriesService {
    private static final Logger log = LoggerFactory.getLogger(CategoriesServiceImpl.class);
    private CategoriesRepository model;

    @Autowired
    public void setModel(CategoriesRepository model) {
        this.model = model;
    }

    @Override
    public ArrayList<Category> find() throws CategoryDAOException {
        ArrayList<Category> categoriesList = new ArrayList<>();
        try {
            ResultSet rs = model.find();
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new CategoryDAOException("Категории не найдены");

            while (rs.next()) {
                bindToCategoryList(categoriesList, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new CategoryDAOException(e.getMessage());
        }
        return categoriesList;
    }

    @Override
    public ArrayList<Category> find(String condition) throws CategoryDAOException {
        ArrayList<Category> categoriesList = new ArrayList<>();
        try {
            ResultSet rs = model.find(condition);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new CategoryDAOException("Категории в базе данных не найдены");

            while (rs.next()) {
                bindToCategoryList(categoriesList, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new CategoryDAOException(e.getMessage());
        }
        return categoriesList;
    }

    @Override
    public Category getCategoryById(int id) throws CategoryDAOException {
        return model.getCategoryById(id);
    }

    @Override
    public int deleteCategory(HttpServletRequest req) throws CategoryDAOException {
        if(req.getParameter("id").equals(""))
            throw new CategoryDAOException("Не получен ID категории");

        int id = Integer.parseInt(req.getParameter("id"));
        int deleted = 0;

        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(req.isUserInRole("ROLE_USER"))
            throw new CategoryDAOException("У вас нет прав на удаление категорий");

        try {
            deleted = model.deleteFirst(id);
        } catch (ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new CategoryDAOException(e.getMessage());
        }
        return deleted;
    }

    @Override
    public long save(HttpServletRequest req) throws CategoryDAOException, ModelException {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(req.isUserInRole("ROLE_USER"))
            throw new CategoryDAOException("У вас нет прав на добавление/изменение категорий");

        if (req.getParameter("name") == null || req.getParameter("name").equals(""))
            throw new CategoryDAOException("Не указано наименование категории");


        if (req.getParameter("description") == null || req.getParameter("description").equals(""))
            throw new CategoryDAOException("Укажите описание категории");


        HashMap<String, Object> data = new HashMap<>();
        for (Object key:
                req.getParameterMap().keySet()) {
            String _key = key.toString();
            String[] value = req.getParameterMap().get(key);
            data.put(_key, value[0]);
        }

        return model.save(data);
    }


    public static void bindToCategoryList(ArrayList<Category> categoriesList, ResultSet result) throws SQLException {
        Category category = new Category();
        bindToCategory(category, result);
        categoriesList.add(category);
    }

    public static void bindToCategory(Category category, ResultSet rs) throws SQLException {
        category.setId(rs.getInt("id"));
        category.setName(rs.getString("name"));
        category.setDescription(rs.getString("description"));
    }

}
