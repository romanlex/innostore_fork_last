package com.innostore.app.services.impl;

import com.innostore.app.enums.Category;
import com.innostore.app.enums.Product;
import com.innostore.app.enums.User;
import com.innostore.app.repository.ProductRepository;
import com.innostore.app.services.ProductService;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProductDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

@Service
public class ProductServiceImpl implements ProductService {
    private static final Logger log = LoggerFactory.getLogger(ProductServiceImpl.class);
    private ProductRepository model;

    @Autowired
    public void setModel(ProductRepository model) {
        this.model = model;
    }

    @Override
    public ArrayList<Product> find() throws ProductDAOException {
        ArrayList<Product> productList = new ArrayList<>();
        try {
            ResultSet rs = model.find();
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new ProductDAOException("Продукты не найдены");

            while (rs.next()) {
                bindToProductList(productList, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ProductDAOException(e.getMessage());
        }
        return productList;
    }

    @Override
    public ArrayList<Product> find(String condition) throws ProductDAOException {
        ArrayList<Product> productList = new ArrayList<>();
        try {
            ResultSet rs = model.find(condition);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new ProductDAOException("Продукты в базе данных не найдены");

            while (rs.next()) {
                bindToProductList(productList, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ProductDAOException(e.getMessage());
        }
        return productList;
    }

    @Override
    public Product getProductById(int id) throws ProductDAOException {
        return model.getProductById(id);
    }

    @Override
    public int deleteProduct(HttpServletRequest req) throws ProductDAOException {
        if(req.getParameter("id").equals(""))
            throw new ProductDAOException("Не получен ID продукта");

        int id = Integer.parseInt(req.getParameter("id"));
        int deleted = 0;

        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(req.isUserInRole("ROLE_USER"))
            throw new ProductDAOException("У вас нет прав на удаление продуктов из каталога");

        try {
            deleted = model.deleteFirst(id);
        } catch (ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ProductDAOException(e.getMessage());
        }
        return deleted;
    }

    @Override
    public long save(HttpServletRequest req) throws ProductDAOException, ModelException {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(req.isUserInRole("ROLE_USER"))
            throw new ProductDAOException("У вас нет прав на добавление/изменение продуктов в каталог");

        if (req.getParameter("name") == null || req.getParameter("name").equals(""))
            throw new ProductDAOException("Не указано наименование товара");


        if (req.getParameter("price") == null || req.getParameter("price").equals(""))
            throw new ProductDAOException("Укажите стоимость товара");


        HashMap<String, Object> data = new HashMap<>();
        for (Object key:
                req.getParameterMap().keySet()) {
            String _key = key.toString();
            String[] value = req.getParameterMap().get(key);
            data.put(_key, value[0]);
        }

        if(data.get("popular") != null)
            data.put("popular", "Y");

        data.putIfAbsent("popular", "N");

        return model.save(data);
    }


    public static void bindToProductList(ArrayList<Product> productList, ResultSet result) throws SQLException {
        Product product = new Product();
        bindToProduct(product, result);
        productList.add(product);
    }

    public static void bindToProduct(Product product, ResultSet rs) throws SQLException {
        Category category = new Category();
        category.setId(rs.getInt("category.id"));
        category.setName(rs.getString("category.name"));
        category.setDescription(rs.getString("category.description"));

        product.setId(rs.getInt("id"));
        product.setArticle(rs.getString("article"));
        product.setCategory(category);
        product.setCategoryId(rs.getInt("categoryId"));
        product.setName(rs.getString("name"));
        product.setLongname(rs.getString("longname"));
        product.setImage(rs.getString("image"));
        product.setVendor(rs.getString("vendor"));
        product.setPrice(rs.getDouble("price"));
        product.setPopular(rs.getString("popular"));
        product.setDescription(rs.getString("description"));
    }



}
