package com.innostore.app.repository;

import com.innostore.app.enums.User;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

public interface UserRepository {
    /**
     * Получает ResultSet всех юзеров из базы данных
     * @return ResultSet
     * @throws ModelException ошибка при поиске записи
     */
    ResultSet find() throws ModelException;

    /**
     * Получает ResultSet по условию condition из базы данных
     * @param condition условия выборки из базы данных
     * @return ResultSet
     * @throws ModelException ошибка при поиске записи
     */
    ResultSet find(String condition) throws ModelException;

    /**
     * Удаление юзера из БД
     * @param id id
     * @return количество затронутых строк в БД
     * @throws ModelException ошибка при запросе в базу
     */
    int deleteFirst(int id) throws ModelException;

    /**
     * Сохранение/обновление юзера в базе
     * @param map data
     * @return количество затронутых строк в БД
     * @throws ModelException ошибка при поиске записи
     */
    long save(HashMap<String, Object> map) throws ModelException;

    boolean userExist(String email) throws UserDAOException;
    User getUserById(int id) throws UserDAOException;
    User getUserByLogin(String email) throws UserDAOException;
    ArrayList<User> getUsersByRole(String role) throws UserDAOException;
}



