package com.innostore.app.models;


import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class OrderStatusesModel extends Model {

    private static final String table = "order_statuses";
    private static Logger log = LoggerFactory.getLogger(OrderStatusesModel.class);

    public OrderStatusesModel() {
        setSource(table);
    }


}
