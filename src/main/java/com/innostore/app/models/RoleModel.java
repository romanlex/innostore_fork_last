package com.innostore.app.models;

import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RoleModel extends Model {

    private static final String table = "users_roles";
    private static Logger log = LoggerFactory.getLogger(RoleModel.class);

    public RoleModel() {
        setSource(table);
    }

}



