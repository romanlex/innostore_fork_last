package com.innostore.app.models;

import com.innostore.app.enums.Product;
import com.innostore.app.repository.ProductRepository;
import com.innostore.app.services.impl.ProductServiceImpl;
import com.innostore.core.common.Settings;
import com.innostore.core.database.Database;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProductDAOException;
import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.*;
import java.util.HashMap;

@Repository
public class ProductModel extends Model implements ProductRepository {
    private static final String table = "products";
    private static Logger log = LoggerFactory.getLogger(ProductModel.class);
    private static final String SQL_FIND_PRODUCTS = "SELECT p.*, category.* FROM " + table + " p LEFT JOIN categories category ON p.categoryId = category.id";
    private static final String SQL_FIND_PRODUCTS_BY_ID = "SELECT p.*, category.* FROM " + table + " p LEFT JOIN categories category ON p.categoryId = category.id WHERE p.id = ? LIMIT 1";

    public ProductModel() {
        setSource(table);
    }

    @PostConstruct
    public void productModelInit(){
        log.info("Initialize ProductModel");
    }

    @Override
    public ResultSet find() throws ModelException {
        ResultSet result;
        Connection db = Database.pool.checkOut();
        try (Statement stmt = db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            if(Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(SQL_FIND_PRODUCTS);

            result = stmt.executeQuery(SQL_FIND_PRODUCTS);
            stmt.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not call find() of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }

    @Override
    public ResultSet find(String condition) throws ModelException {
        ResultSet result;
        Connection db = Database.pool.checkOut();
        try (Statement stmt = db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            String query = SQL_FIND_PRODUCTS + " WHERE " + condition;
            if(Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(query);

            result = stmt.executeQuery(query);
            stmt.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not call find(condition) of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }

    @Override
    public Product getProductById(int id) throws ProductDAOException {
        Product product = new Product();
        try {
            ResultSet rs = this.findFirst(id);
            int rows = 0;

            if (rs.last()) {
                rows = rs.getRow();
                rs.beforeFirst();
            }

            if(rows == 0)
                throw new ProductDAOException("Продукт с данным ID (" + id + ") не найден");

            while (rs.next()) {
                ProductServiceImpl.bindToProduct(product, rs);
            }
        } catch (SQLException | ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ProductDAOException(e.getMessage());
        }
        return product;
    }

    @Override
    public int deleteFirst(int id) throws ModelException {
        return this._deleteFirst(id);
    }

    @Override
    public long save(HashMap<String, Object> map) throws ModelException {
        return this._save(map);
    }


    public ResultSet findFirst(int id) throws ModelException {
        ResultSet result;
        Connection db = Database.pool.checkOut();
        try (Statement stmt = db.createStatement()) {
            PreparedStatement ps = db.prepareStatement(SQL_FIND_PRODUCTS_BY_ID);
            ps.setInt(1, id);

            if(Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(SQL_FIND_PRODUCTS_BY_ID);

            result = ps.executeQuery();
            ps.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not findFirst() method of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }

}



