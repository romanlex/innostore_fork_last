package com.innostore.app.models;


import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class OrderProductsModel extends Model {

    private static final String table = "order_products";
    private static Logger log = LoggerFactory.getLogger(OrderProductsModel.class);

    public OrderProductsModel() {
        setSource(table);
    }

}
