package com.innostore.app.models;

import com.innostore.app.enums.User;
import com.innostore.app.repository.UserRepository;
import com.innostore.app.services.impl.UserServiceImpl;
import com.innostore.core.database.Database;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;
import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

@Repository
public class UserModel extends Model implements UserRepository {
    private static final String table = "users";
    private static Logger log = LoggerFactory.getLogger(UserModel.class);

    private static final String FIND_USER_BY_EMAIL_SQL = "SELECT `users`.*, GROUP_CONCAT(DISTINCT `users_roles`.role SEPARATOR ',') AS roles " +
            "FROM `users` " +
            "LEFT JOIN `users_roles` " +
            "ON `users`.id = `users_roles`.userId " +
            "WHERE `users`.email = ? " +
            "GROUP BY `users`.`id`";

    private static final String FIND_USER_BY_ID_SQL = "SELECT `users`.*, GROUP_CONCAT(DISTINCT `users_roles`.role SEPARATOR ',') AS roles " +
            "FROM `users` " +
            "LEFT JOIN `users_roles` " +
            "ON `users`.id = `users_roles`.userId " +
            "WHERE `users`.id = ? " +
            "GROUP BY `users`.`id`";

    private static final String FIND_USER_BY_ROLE_SQL = "SELECT * FROM users WHERE role = ?";

    public UserModel() {
        setSource(table);
        String[] many = {
                "id",
                "Order",
                "userId"
        };
        hasMany(many);
    }

    @PostConstruct
    public void userModelInit(){
        log.info("Initialize UserModel");
    }

    @Override
    public ResultSet find() throws ModelException {
        return this._find();
    }

    @Override
    public ResultSet find(String condition) throws ModelException {
        return this._find(condition);
    }

    @Override
    public int deleteFirst(int id) throws ModelException {
        return this._deleteFirst(id);
    }

    @Override
    public long save(HashMap<String, Object> map) throws ModelException {
        return this._save(map);
    }

    @Override
    public boolean userExist(String email) throws UserDAOException {
        int rows = 0;
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_EMAIL_SQL)) {
            ps.setString(1, email);
            ResultSet result = ps.executeQuery();

            if (result.last()) {
                rows = result.getRow();
                result.beforeFirst();
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        Database.pool.checkIn(db);
        return (rows > 0);
    }

    @Override
    public User getUserById(int id) throws UserDAOException {
        User user = new User();
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_ID_SQL)){
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserServiceImpl.bindToUser(user, rs);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        return user;
    }

    @Override
    public User getUserByLogin(String email) throws UserDAOException {
        User user = new User();
        user.setId(0);
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_EMAIL_SQL)) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserServiceImpl.bindToUser(user, rs);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        Database.pool.checkIn(db);
        return user;
    }



    @Override
    public ArrayList<User> getUsersByRole(String role) throws UserDAOException {
        ArrayList<User> userlist = new ArrayList<>();
        Connection db = Database.pool.checkOut();
        try (PreparedStatement ps = db.prepareStatement(FIND_USER_BY_ROLE_SQL)) {
            ps.setString(1, role);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserServiceImpl.bindToUserList(userlist, rs);
            }
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
        Database.pool.checkIn(db);
        return userlist;
    }

}



