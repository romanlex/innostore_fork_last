package com.innostore.app.controllers.mgr;

import com.innostore.app.enums.Category;
import com.innostore.app.enums.Product;
import com.innostore.app.services.CategoriesService;
import com.innostore.app.services.ProductService;
import com.innostore.core.exceptions.CategoryDAOException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProductDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

@Controller
public class ProductsController {
    private static final Logger log = LoggerFactory.getLogger(ProductsController.class);

    private ProductService productService;
    private CategoriesService categoriesService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    @Autowired
    public void setCategoriesService(CategoriesService categoriesService) {
        this.categoriesService = categoriesService;
    }

    /**
     * ДОБАВЛЕНИЕ ТОВАРА
     */
    @RequestMapping(value="/product/add", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String addAction(Model model) {
        try {
            ArrayList<Category> categories = categoriesService.find();
            model.addAttribute("categories", categories);
        } catch (CategoryDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
            return "manager/mgr_catalog";
        }
        return "manager/mgr_product_add";
    }

    @RequestMapping(value="/product/add", method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String addPostAction(HttpServletRequest req, HttpSession session, Model model) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        try {
            if (productService.save(req) > 0) {
                return "redirect:/catalog";
            } else {
                model.addAttribute("error", "Произошла ошибка при добавлении товара. Ни одна запись в БД не была сохранена.");
                return "manager/mgr_product_add";
            }
        } catch (ModelException | ProductDAOException e) {
            log.error(e.getMessage(),e.getCause());
            model.addAttribute("error", "Произошла ошибка при сохранении товара: " + e.getMessage());
            return "manager/mgr_product_add";
        }
    }


    /**
     * УДАЛЕНИЕ ТОВАРА
     */
    @RequestMapping(value={"/product/delete/*", "/product/delete/", "/product/delete"}, method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String deletePostAction(HttpServletRequest req, HttpSession session, Model model) {
        try {
            if (productService.deleteProduct(req) > 0) {
                return "redirect:/catalog";
            } else {
                model.addAttribute("error", "Продукт с данным ID не найден.");
            }
        } catch (ProductDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_catalog";
    }


    /**
     * РЕДАКТИРОВАНИЕ ТОВАРОВ
     */
    @RequestMapping(value={"/product/edit/", "/product/edit"} , method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String editActionWrong(HttpSession session, Model model) {
        model.addAttribute("error", "Укажите ID продукта");
        return "manager/mgr_catalog";
    }

    @RequestMapping(value={"/product/edit/{idString}"} , method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String editAction(@PathVariable String idString, Model model) {
        if(idString == null || idString.equals(""))
        {
            model.addAttribute("error", "Укажите ID продукта");
            return "manager/mgr_catalog";
        }

        int id = Integer.parseInt(idString);

        try {
            Product product = productService.getProductById(id);
            ArrayList<Category> categories = categoriesService.find();
            model.addAttribute("product", product);
            model.addAttribute("categories", categories);
        } catch (ProductDAOException | CategoryDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
            return "manager/mgr_catalog";
        }
        return "manager/mgr_product_edit";
    }

    @RequestMapping(value={"/product/edit/*", "/product/edit/", "/product/edit"}, method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String editPostAction(HttpServletRequest req, HttpSession session, Model model) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        try {
            if (productService.save(req) > 0) {
                Product updatedProduct = productService.getProductById(Integer.parseInt(req.getParameter("id")));
                ArrayList<Category> categories = categoriesService.find();
                model.addAttribute("product", updatedProduct);
                model.addAttribute("categories", categories);
                model.addAttribute("success", "Информация о товаре успешно обновлёна");
            } else {
                model.addAttribute("error", "Произошла ошибка при обновлении товара. Ни одна запись в БД не была обновлена.");
            }
        } catch (ModelException | ProductDAOException | CategoryDAOException e) {
            log.error(e.getMessage(),e.getCause());
            model.addAttribute("error", "Произошла ошибка при обновлении товара: " + e.getMessage());
        }
        return "manager/mgr_product_edit";
    }
}
