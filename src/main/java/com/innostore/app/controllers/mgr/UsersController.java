package com.innostore.app.controllers.mgr;

import com.innostore.app.enums.User;
import com.innostore.app.services.UserService;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

@Controller
public class UsersController {
    private static final Logger log = LoggerFactory.getLogger(UsersController.class);
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value="/users", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String indexAction(Model model) {

        ArrayList<User> userList;
        try {
            userList = userService.find();
            model.addAttribute("users", userList);
        } catch (UserDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_users";
    }

    /**
     * УДАЛЕНИЕ ЮЗЕРА
     */
    @RequestMapping(value={"/users/delete/*", "/users/delete/", "/users/delete"}, method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String deletePostAction(HttpServletRequest req, HttpSession session, Model model) {
        try {
            if (userService.deleteUser(req) > 0) {
                return "redirect:/users";
            } else {
                model.addAttribute("error", "Пользователь с данным ID не найден.");
            }
        } catch (UserDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_catalog";
    }

    /**
     * РЕДАКТИРОВАНИЕ ЮЗЕРА
     */
    @RequestMapping(value={"/users/edit/", "/users/edit"} , method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String editActionWrong(HttpSession session, Model model) {
        model.addAttribute("error", "Укажите ID юзера");
        return "manager/mgr_users";
    }

    @RequestMapping(value="/users/edit/{idString}", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String editAction(@PathVariable String idString, Model model) {
        if(idString == null || idString.equals(""))
        {
            model.addAttribute("error", "Укажите ID пользователя");
            return "manager/mgr_users";
        }

        int id = Integer.parseInt(idString);
        try {
            User user = userService.getUserById(id);
            model.addAttribute("user", user);
        } catch (UserDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_user_edit";
    }

    @RequestMapping(value={"/users/edit/*", "/users/edit/", "/users/edit"}, method = RequestMethod.POST)
    @Secured("ROLE_ADMIN")
    public String editPostAction(HttpServletRequest req, HttpSession session, Model model) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        try {
            if (userService.save(req) > 0) {
                User updatedUser = userService.getUserById(Integer.parseInt(req.getParameter("id")));
                model.addAttribute("user", updatedUser);
                model.addAttribute("success", "Профиль пользователя обновлён");
            } else {
                model.addAttribute("error", "Произошла ошибка при обновлении профиля пользователя. Попробуйте позднее.");
            }
        } catch (ModelException | UserDAOException e) {
            log.error(e.getMessage(),e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return "manager/mgr_user_edit";

    }

}
