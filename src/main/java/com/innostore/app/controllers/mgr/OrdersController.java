package com.innostore.app.controllers.mgr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class OrdersController {
    private static final Logger log = LoggerFactory.getLogger(OrdersController.class);

    @RequestMapping(value="/orders", method = RequestMethod.GET)
    @Secured("ROLE_ADMIN")
    public String indexAction() {
        return "manager/mgr_orders";
    }
}
