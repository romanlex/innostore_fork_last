package com.innostore.app.controllers.web;

import com.innostore.app.enums.User;
import com.innostore.app.services.ProfileService;
import com.innostore.app.services.UserService;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.ProfileDAOException;
import com.innostore.core.exceptions.UserDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;

@Controller
public class ProfileController {
    private static final Logger log = LoggerFactory.getLogger(ProfileController.class);

    private ProfileService profileService;
    private UserService userService;

    @Autowired
    public void setProfileService(ProfileService profileService) {
        this.profileService = profileService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value="/profile", method = RequestMethod.GET)
    public String indexAction(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int userId = user.getId();
        try {
            model.addAttribute("user", userService.getUserById(userId));
        } catch (UserDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", "Ошибка при выполнении запроса: " + e.getMessage());
        }
        return "profile";
    }

    @RequestMapping(value="/profile", method = RequestMethod.POST)
    public String indexPostAction(HttpServletRequest req, HttpSession session, Model model) throws UnsupportedEncodingException {
        req.setCharacterEncoding("UTF-8");
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        int userId = user.getId();

        try {
            if (profileService.save(req) > 0) {
                User updatedUser = userService.getUserById(userId);
                session.setAttribute("user", updatedUser);
                model.addAttribute("user", updatedUser);
                model.addAttribute("success", "Профиль обновлён.");
            } else {
                model.addAttribute("error", "Произошла ошибка при обновлении вашего профиля. Попробуйте позднее.");
                return "forward:profile";
            }
        } catch ( ModelException | ProfileDAOException | UserDAOException e) {
            log.error(e.getMessage(),e.getCause());
            model.addAttribute("error", e.getMessage());
            return "forward:profile";
        }
        return "profile";
    }

}
