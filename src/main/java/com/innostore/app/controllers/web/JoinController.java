package com.innostore.app.controllers.web;

import com.innostore.app.repository.UserConfirmationRepository;
import com.innostore.app.services.UserService;
import com.innostore.core.common.Settings;
import com.innostore.core.common.Utils;
import com.innostore.core.exceptions.CoreException;
import com.innostore.core.exceptions.ModelException;
import com.innostore.core.exceptions.UserDAOException;
import com.innostore.core.mailer.Mail;
import org.apache.commons.validator.routines.EmailValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

@Controller
public class JoinController  {
    private static final Logger log = LoggerFactory.getLogger(JoinController.class);
    private UserConfirmationRepository model;
    private UserService userService;

    @Autowired
    public void setModel(UserConfirmationRepository model) {
        this.model = model;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value="/join", method = RequestMethod.GET)
    @Secured("ROLE_ANONYMOUS")
    public String indexAction(HttpSession session, Model model) {
        return "join";
    }

    @RequestMapping(value="/join", method = RequestMethod.POST)
    @Secured("ROLE_ANONYMOUS")
    public String indexPostAction(HttpServletRequest req, HttpSession session, Model model) {
        String email = req.getParameter("email");

        EmailValidator emailValidator = EmailValidator.getInstance();
        if(!emailValidator.isValid(email))
        {
            model.addAttribute("error", "Email указан не корректно. Укажите другой действующий email.");
            return "join";
        }

        String password = req.getParameter("password");
        String password2 = req.getParameter("password2");

        if(password.equals("")) {
            model.addAttribute("error", "Укажите пароль");
            return "join";
        }
        if(password2.equals("")) {
            model.addAttribute("error", "Повторите пароль");
            return "join";
        }

        if(!password.equals(password2)) {
            model.addAttribute("error", "Пароли не совпадают");
            return "join";
        }

        String firstname = req.getParameter("firstname");
        if(firstname.equals("")) {
            model.addAttribute("error", "Укажите ваше имя");
            return "join";
        }


        try {
            long insertedId = userService.join(email, password, firstname);
            if(insertedId > 0){
                log.info("New user ({}) is joined to application.", email);
                if(Integer.parseInt(Settings.getSettings(true).getProperty("registration.confirm.email")) == 0) {
                    model.addAttribute("success", "Вы успешно зарегистрировались. Можете авторизоваться с вашим логином и паролем.");
                    return "join";
                } else {
                    // Спросить Артёма
                    Thread confirmProcess = new Thread(() -> {
                        try {
                            startConfirmationProcess(email, password, insertedId, req);
                        } catch (CoreException | UserDAOException e) {
                            log.error(e.getMessage(), e.getCause());
                            log.error("Cannot finish confirmation process: " + e.getMessage());
                        }
                    });
                    confirmProcess.start();
                    model.addAttribute("warning", "Для завершения регистрации вам необходимо активировать аккаунт. Проверьте ваш адрес элекронной почты.");
                    return "join";
                }
            } else {
                log.error("Cannot save user to database. executeUpdate return 0. Data of form: {}", Arrays.asList(email, password, firstname));
                model.addAttribute("error", "Не удалось зарегистрировать пользователя. Попробуйте позднее.");
                return "join";
            }
        } catch (CoreException | ModelException | UserDAOException e) {
            log.error(e.getMessage());
            model.addAttribute("error", e.getMessage());
        }
        return "join";
    }

    private void startConfirmationProcess(String email, String password, long userId, HttpServletRequest req) throws CoreException, UserDAOException {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        String md5String = email + password + userId;
        String code = Utils.getMd5String(md5String);
        String confirmLink = Utils.getBaseUrl(req) + "/confirm/" + code;

        try {
            HashMap<String, Object> data = new HashMap<>();
            data.put("userId", userId);
            data.put("code", code);
            data.put("date", timestamp);
            data.put("confirmed", "N");
            if( model.save(data) > 0 ) {
                String txtMessage = "Здравствуйте. Для завершения регистрации вам необходимо подтвердить аккаунт. Пройдите по ссылке:\n"
                        + confirmLink + "\n";

                String htmlMessage = "Здравствуйте. Для завершения регистрации вам необходимо подтвердить аккаунт. Пройдите по ссылке:<br />"
                        + "<a href=\"" + confirmLink + "\">" + confirmLink + "</a><br />";

                if (Mail.send(String.join(",", email), "Подтверждение регистрации", txtMessage, htmlMessage))
                    log.info("Confirmation message for user (#{}) with email <{}> successfully sended.", userId, email);
                else
                    log.error("Cannot send confirmation message for for user (#{}) with email <{}>. Email not sended.", userId, email);
            } else {
                throw new UserDAOException("К сожалению не удалось запустить процесс подтверждения.");
            }
        } catch (ModelException e) {
            log.error(e.getMessage(), e.getCause());
            throw new UserDAOException(e.getMessage());
        }
    }


}
