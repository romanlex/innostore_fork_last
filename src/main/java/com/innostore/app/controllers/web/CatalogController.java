package com.innostore.app.controllers.web;

import com.innostore.app.enums.Product;
import com.innostore.app.enums.Role;
import com.innostore.app.enums.User;
import com.innostore.app.services.ProductService;
import com.innostore.core.exceptions.ProductDAOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;


@Controller
public class CatalogController {
    private static final Logger log = LoggerFactory.getLogger(CatalogController.class);

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value="/catalog", method = RequestMethod.GET)
    public String indexAction(HttpServletRequest request, HttpSession session, Model model) {
        ArrayList<Product> productList;
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Role> roles = user.getAuthorities();
        String jsp = (request.isUserInRole("ROLE_USER") ? "catalog" : "manager/mgr_catalog");
        try {
            productList = productService.find();
            model.addAttribute("products", productList);
        } catch (ProductDAOException e) {
            log.error(e.getMessage(), e.getCause());
            model.addAttribute("error", e.getMessage());
        }
        return jsp;
    }
}
