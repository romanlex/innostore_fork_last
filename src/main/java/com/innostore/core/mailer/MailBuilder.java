package com.innostore.core.mailer;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import java.nio.file.Path;
import java.util.List;

public class MailBuilder {

    public Multipart build(String messageText, String messageHtml, List<Path> attachments) throws MessagingException {

        Multipart mpMixed = new MimeMultipart("mixed");
        Multipart mpMixedAlternative = setPart(mpMixed, "alternative");

        addTextVersion(mpMixedAlternative, messageText);
        addHtmlVersion(mpMixedAlternative, messageHtml);
        addAttachments(mpMixed, attachments);

        return mpMixed;
    }

    private Multipart setPart(Multipart parent, String alternative) throws MessagingException {
        MimeMultipart child =  new MimeMultipart(alternative);
        final MimeBodyPart mbp = new MimeBodyPart();
        parent.addBodyPart(mbp);
        mbp.setContent(child);
        return child;
    }

    private void addTextVersion(Multipart mpRelatedAlternative, String messageText) throws MessagingException {
        final MimeBodyPart textPart = new MimeBodyPart();
        textPart.setContent(messageText, "text/plain; charset=UTF-8");
        mpRelatedAlternative.addBodyPart(textPart);
    }

    private void addHtmlVersion(Multipart parent, String messageHtml) throws MessagingException {
        // HTML version
        Multipart mpRelated = setPart(parent,"related");

        // Html
        MimeBodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(messageHtml, "text/html; charset=UTF-8");
        mpRelated.addBodyPart(htmlPart);

    }

    private void addAttachments(Multipart parent, List<Path> attachments) throws MessagingException {
        if (attachments != null)
        {
            for (Path attachment : attachments)
            {
                MimeBodyPart mbpAttachment = new MimeBodyPart();
                DataSource source = new FileDataSource(attachment.toString());
                mbpAttachment.setDataHandler(new DataHandler(source));
                mbpAttachment.setDisposition(BodyPart.ATTACHMENT);
                mbpAttachment.setFileName(source.getName());
                parent.addBodyPart(mbpAttachment);
            }
        }
    }
}
