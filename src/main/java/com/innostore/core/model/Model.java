package com.innostore.core.model;

import com.innostore.core.common.Settings;
import com.innostore.core.database.Database;
import com.innostore.core.exceptions.ModelException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Model {
    private static final Logger log = LoggerFactory.getLogger(Model.class);
    private String[] relation = null;
    private String tablename;

    /**
     * Save object to database
     * @param map map with key value pair
     * @return long LAST_INSERT_ID
     * @throws ModelException Error in sql
     */
    public long _save(HashMap<String, Object> map) throws ModelException {
        Connection db = Database.pool.checkOut();
        boolean update = false;
        ResultSet generatedKeys;
        long insertedId = -1;
        int rowExistedId = 0;

        try {
            if (map.get("id") != null) {
                rowExistedId = Integer.parseInt(map.get("id").toString());
                ResultSet rs = _findFirst(rowExistedId);
                int rows = 0;

                if (rs.last()) {
                    rows = rs.getRow();
                    rs.beforeFirst();
                }

                if (rows > 0) {
                    update = true;
                }
            }

            List<String> keys = new ArrayList<>(map.keySet());
            List<String> questions = new ArrayList<>(map.keySet());
            questions.replaceAll(s -> s != null ? "?" : "");
            String fields = String.join(", ", keys);
            String _values = String.join(", ", questions);
            String query;
            if (!update)
                query = "INSERT INTO " + tablename + " (" + fields + ") VALUES (" + _values + ");";
            else {
                keys.remove("id");
                map.remove("id");
                List<String> sql = new ArrayList<>();
                for (String key :
                        keys) {
                    sql.add(key + "=?");
                }
                String _fieldvalues = String.join(", ", sql);
                query = "UPDATE " + tablename + " SET  " + _fieldvalues + " WHERE id = " + rowExistedId + ";";
            }
            PreparedStatement preparedStatement = db.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

            int i = 1;
            for (String key :
                    keys) {

                Object value = (map.get(key) != null ? map.get(key) : "");
                String type = value.getClass().getSimpleName();
                switch (type) {
                    case "String":
                        preparedStatement.setString(i, value.toString());
                        break;
                    case "Double":
                        preparedStatement.setDouble(i, Double.parseDouble(value.toString()));
                        break;
                    case "Float":
                        preparedStatement.setFloat(i, Float.parseFloat(value.toString()));
                        break;
                    case "Integer":
                        preparedStatement.setInt(i, Integer.parseInt(value.toString()));
                        break;
                    case "Long":
                        preparedStatement.setLong(i, Long.parseLong(value.toString()));
                        break;
                    case "Date":
                        preparedStatement.setDate(i, java.sql.Date.valueOf(value.toString()));
                        break;
                    case "Timestamp":
                        preparedStatement.setTimestamp(i, Timestamp.valueOf(value.toString()));
                        break;
                }
                i++;
            }

            if (Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(preparedStatement.toString());

            int affectedRows = preparedStatement.executeUpdate();

            if (update)
                return affectedRows;

            if (affectedRows == 0) {
                throw new SQLException("Creating " + this.getClass() + " failed, no rows affected.");
            }

            generatedKeys = preparedStatement.getGeneratedKeys();

            if (generatedKeys.next()) {
                insertedId = generatedKeys.getLong(1);
            } else
                throw new SQLException("Creating " + this.getClass() + " failed, no ID obtained.");
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException(e.getMessage());
        }
        Database.pool.checkIn(db);
        return insertedId;
    }

    /**
     * Find first object by ID
     * @param id find row by id
     * @return ResultSet
     * @throws ModelException sql errors
     */
    public ResultSet _findFirst(int id) throws ModelException {
        ResultSet result = null;
        Connection db = Database.pool.checkOut();
        try (Statement stmt = db.createStatement()) {
            String query = "SELECT * FROM " + tablename + " WHERE `id` = ? LIMIT 1";
            PreparedStatement ps = db.prepareStatement(query);
            ps.setInt(1, id);

            if(Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(query);

            result = ps.executeQuery();
            ps.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not save() method of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }

    /**
     * Find all objects
     * @return ResultSet
     * @throws ModelException sql errors
     */
    public ResultSet _find() throws ModelException {
        ResultSet result = null;
        Connection db = Database.pool.checkOut();
        try (Statement stmt = db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            String query = "SELECT * FROM " + tablename;
            if(Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(query);

            result = stmt.executeQuery(query);
            stmt.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not call find() of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }

    /**
     * Find objects by condition
     * @return ResultSet
     * @throws ModelException sql errors
     */
    public ResultSet _find(String condition) throws ModelException {
        ResultSet result = null;
        Connection db = Database.pool.checkOut();
        try (Statement stmt = db.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY)) {
            String query = "SELECT * FROM " + tablename + " WHERE " + condition;
            if(Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(query);

            result = stmt.executeQuery(query);
            stmt.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not call find(condition) of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }

    /**
     * Delete first row by id
     * @param id id of row
     * @return количество затронутых элементов
     * @throws ModelException ошибка запроса
     */
    public int _deleteFirst(int id) throws ModelException {
        int result = 0;
        Connection db = Database.pool.checkOut();
        try {
            Statement query = db.createStatement();
            String sql = "DELETE FROM " + tablename + " WHERE id = ?";
            PreparedStatement preparedStatement = db.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            if (Integer.parseInt(Settings.getSettings().getProperty("application.log.sql")) == 1)
                log.debug(preparedStatement.toString());

            result = preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e.getCause());
            throw new ModelException("Could not call delete() method of model: " + e.getMessage());
        }
        Database.pool.checkIn(db);
        return result;
    }


    protected void setSource(String table){
        tablename = Settings.getSettings().getProperty("database.table_prefix") + table;
    }

    protected void hasMany(String[] many) {
        relation = many;
    }
    protected void hasManyToMany(String[] manyToMany) {
        relation = manyToMany;
    }
    protected boolean hasRelations() {
        return (relation != null);
    }
    protected String[] getRelations() {
        return relation;
    }

    private static String getCallerClassName() {
        StackTraceElement[] stElements = Thread.currentThread().getStackTrace();
        for (int i=1; i<stElements.length; i++) {
            StackTraceElement ste = stElements[i];
            if (!ste.getClassName().equals(Model.class.getName()) && ste.getClassName().indexOf("java.lang.Thread")!=0) {
                return ste.getClassName();
            }
        }
        return null;
    }
}
