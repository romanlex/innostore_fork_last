package com.innostore.core.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResultSetMapper<T> {

    private static final Logger log = LoggerFactory.getLogger(ResultSetMapper.class);
    public HashMap<List, Integer> map = new HashMap<>();

    private void setProperty(Object clazz, String fieldName, Object columnValue) {
        try {
            Field field = clazz.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(clazz, columnValue);
        } catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
            log.error("Error when trying set property: " + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public List<T> mapRersultSetToObject(ResultSet rs, Class outputClass, String[] relation) {
        /* Fucking code, spent two days
         relation[0] - column in main model
         relation[1] - table for relation
         relation[2] - column in relation table
         */
        List<T> outputList = null;
        try {
            // ResultSet не должен быть пустым
            if (rs != null) {
                ResultSetMetaData rsmd = rs.getMetaData();
                // Получаем все поля класс
                Field[] fields = outputClass.getDeclaredFields();
                List<T> collection = new ArrayList<T>();
                Integer keyMapValue = 0;
                while (rs.next()) {
                    T obj = (T) outputClass.newInstance();
                    boolean setToMap = false;
                    keyMapValue = 0;
                    for (int _iterator = 0; _iterator < rsmd.getColumnCount(); _iterator++) {
                        // Получаем имя SQL поля
                        String columnName = rsmd.getColumnName(_iterator + 1);
                        // Получаем значение поля
                        Object columnValue = rs.getObject(_iterator + 1);

                        // пробегаемся по всем атрибутам(полям) класс и проверяем имеется ли у них анотация для последующего сравнения с именем SQL поля
                        for (Field field : fields) {
//                            Column column = field.getAnnotation(Column.class);
//                            if(relation.length > 0 && columnName.equals(relation[2])) {
//                                setToMap = true;
//                                keyMapValue = (Integer) columnValue;
//                            }
//                            if (column.name().equalsIgnoreCase(columnName) && columnValue != null) {
//                                BeanUtils.setProperty(obj, field.getName(), columnValue);
//                                break;
//                            }
                        }
                    }

                    // Добавляем в коллекцию
                    if (setToMap)
                        collection.add(obj);

                    if (outputList == null) {
                        outputList = new ArrayList<T>();
                    }


                    outputList.add(obj);
                }

                map.put(collection, keyMapValue);

            } else {
                return null;
            }
        } catch (IllegalAccessException | InstantiationException e) {
            log.error("Fatal error: " + e.getMessage());
        } catch (SQLException e) {
            log.error("SQL Error when trying convert resultset to map property: " + e.getMessage());
        }
        return outputList;
    }

    public HashMap getMap() {
        return map;
    }
}