package com.innostore.core.database;

import com.innostore.core.common.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class Database {
    public static JDBCConnectionPool pool;
    private static final Logger log = LoggerFactory.getLogger(Database.class);

    private JDBCConnectionPool newConnection() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            Properties props = Settings.getSettings();
            String connectionString = "jdbc:";
            connectionString += props.getProperty("database.driver").trim() + "://";
            connectionString += props.getProperty("database.server").trim();
            connectionString += ( (props.getProperty("database.port") != null) ? ":" + props.getProperty("database.port").trim() + "/" : "/" );
            connectionString += props.getProperty("database.dbname").trim();
            connectionString += "?useUnicode=true&characterEncoding=UTF-8";
            log.debug("connect to database with string: {}", connectionString);
            pool = new JDBCConnectionPool("org.mariadb.jdbc.Driver", connectionString, props.getProperty("database.user").trim(), props.getProperty("database.password").trim());
        } catch (ClassNotFoundException e) {
            log.error("Cannot initialize database connection: {}", e.getMessage());
        }
        return null;
    }

    public JDBCConnectionPool getConnection() {
        if(pool == null) {
            return newConnection();
        } else {
            return pool;
        }
    }

    public static class dbholder {
        private final static Database INSTANCE = new Database();

        public static Database getInstance() {
            return INSTANCE;
        }
    }

//    public void returnConnection(Connection con) {
//        // Create the ConnectionPool:
//        JDBCConnectionPool pool = new JDBCConnectionPool(
//                "org.hsqldb.jdbcDriver", "jdbc:hsqldb://localhost/mydb",
//                "sa", "secret");
//
//        // Get a connection:
//        Connection con = pool.checkOut();
//
//        // Use the connection
//    ...
//
//        // Return the connection:
//        pool.checkIn(con);
//    }
}
