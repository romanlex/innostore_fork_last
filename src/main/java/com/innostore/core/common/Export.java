package com.innostore.core.common;

import com.innostore.core.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class Export {
    private Object objects;
    private String table;
    private Model model;
    private File file;
    private static final Logger log = LoggerFactory.getLogger(Export.class);

    public Export(Object objects, Model model, String table, File file) {
        this.objects = objects;
        this.table = table;
        this.model = model;
        this.file = file;
    }

    public void start() {
        try {
            Map<String, Object> properties = new HashMap<String, Object>(1);
            JAXBContext jaxbContext = JAXBContext.newInstance(objects.getClass());
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            if(objects != null) {
                jaxbMarshaller.marshal(objects, this.file);
                log.info("You can get export file from {}", this.file.getAbsolutePath());
            } else {
               log.error("Object is empty. Please check if database table is empty. " + table);
            }

        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
