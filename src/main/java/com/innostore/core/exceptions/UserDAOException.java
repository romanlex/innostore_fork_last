package com.innostore.core.exceptions;

public class UserDAOException extends Exception {
    public UserDAOException(String s) {
        super(s);
    }

}
