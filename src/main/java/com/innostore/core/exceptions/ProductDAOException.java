package com.innostore.core.exceptions;

public class ProductDAOException extends Exception {
    public ProductDAOException(String s) {
        super(s);
    }

}
