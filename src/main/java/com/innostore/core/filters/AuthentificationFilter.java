package com.innostore.core.filters;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AuthentificationFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession(false);
        String authUrl = req.getContextPath() + "/login";
        String joinUrl = req.getContextPath() + "/join";
        String confirmUrl = req.getContextPath() + "/confirm";

        boolean sessionExist = session != null && session.getAttribute("user") != null;
        boolean loginRequest = req.getRequestURI().equals(authUrl);
        boolean joinRequest = req.getRequestURI().equals(joinUrl);
        boolean confirmRequest = req.getRequestURI().contains(confirmUrl);

        if (sessionExist || loginRequest || joinRequest || confirmRequest) {
            chain.doFilter(request, response);
        } else {
            resp.sendRedirect(authUrl);
        }
    }

    @Override
    public void destroy() {

    }
}
