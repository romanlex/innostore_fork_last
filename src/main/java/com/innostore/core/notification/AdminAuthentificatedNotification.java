package com.innostore.core.notification;

import com.innostore.app.enums.Role;
import com.innostore.app.enums.User;
import com.innostore.app.services.UserService;
import com.innostore.core.common.Settings;
import com.innostore.core.exceptions.UserDAOException;
import com.innostore.core.mailer.Mail;
import eu.bitwalker.useragentutils.UserAgent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class AdminAuthentificatedNotification {
    private static final Logger log = LoggerFactory.getLogger(AdminAuthentificatedNotification.class);

    public static void notifySuperAdmin(UserService userService, User authUser, HttpServletRequest req) {
        List<Role> roles = authUser.getAuthorities();
        if(roles.equals("ROLE_SUPERADMIN"))
            return;

        log.info("admin.auth.notification set to: " + Settings.getSettings(true).getProperty("admin.auth.notification"));
        if(Integer.parseInt(Settings.getSettings(true).getProperty("admin.auth.notification")) != 1)
            return;

        log.info("Send notify to super admins");

        ArrayList<User> adminList = null;
        try {
            adminList = userService.getUsersByRole("SuperAdmin");
        } catch ( UserDAOException e) {
            log.error(e.getMessage(), e.getCause());
        }

        if(adminList.size() == 0)
        {
            log.info("Cannot send notification because mail user list is empty.");
            return;
        }

        ArrayList<String> too = new ArrayList<>();
        for (User admin:
             adminList) {
            too.add(admin.getFirstname() + " <" + admin.getEmail() + ">");
        }

        UserAgent userAgent = UserAgent.parseUserAgentString(req.getHeader("User-Agent"));


        String txtMessage = "Зафиксирован вход в аккаунт с правами администратора.\n" +
                "Логин: " + authUser.getEmail() + "\n" +
                "IP адрес: " + req.getRemoteAddr() + "\n" +
                "Браузер: " + userAgent.getBrowser().getName() + " " + userAgent.getBrowserVersion() + "\n" +
                "Юзер агент: " + req.getHeader("User-Agent");

        String htmlMessage = "Зафиксирован вход в аккаунт с правами администратора.<br><br>" +
                "Логин: " + authUser.getEmail() + "<br>" +
                "IP адрес: " + req.getRemoteAddr() + "<br>" +
                "Браузер: " + userAgent.getBrowser().getName() + " " + userAgent.getBrowserVersion() + "<br>" +
                "Юзер агент: " + req.getHeader("User-Agent");

        try {
            Telegram.sendMessage(txtMessage);
        } catch (MalformedURLException | UnsupportedEncodingException e) {
            log.error(e.getMessage(), e.getCause());
        }

        if(Mail.send(String.join(",", too), "Пользователь с ролью Admin авторизовался на сайте", txtMessage, htmlMessage))
            log.info("Notification for super admin successfully sended");
        else
            log.error("Cannot send notification for super admin. Email not sended");

    }
}
