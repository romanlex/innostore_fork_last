package com.innostore.core.notification;

import com.innostore.core.common.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

public class Telegram {
    private static final Logger log = LoggerFactory.getLogger(Telegram.class);

    public static void sendMessage(String message) throws MalformedURLException, UnsupportedEncodingException {
        String apiToken = Settings.getSettings().getProperty("telegram.token");
        String chatId = Settings.getSettings().getProperty("telegram.chat_id");
        String _url = "https://api.telegram.org/bot" + apiToken + "/sendMessage?chat_id=" + chatId + "&text=" + URLEncoder.encode(message, "UTF-8");
        URL url = new URL(_url);
        try {
            URLConnection con = url.openConnection();
            con.setRequestProperty("Content-Type", "application/json");
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null)
                log.info(inputLine);
            in.close();
        } catch (IOException e) {
            log.error(e.getMessage(), e.getCause());
        }

    }
}
