<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Контакты</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="./includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="./includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page delivery">
                    <div class="page__title">
                        <div class="page__title__left">
                            <h1>Информация по доставке</h1>
                        </div>
                        <div class="page__title__right"></div>
                    </div>

                    <div>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus amet animi beatae ducimus,
                        eum excepturi expedita inventore labore laudantium odit perspiciatis possimus recusandae
                        repudiandae sed, suscipit tempore ut. Ducimus, voluptas.
                    </div>
                    <div>Animi distinctio, doloribus incidunt magni minima neque nobis praesentium temporibus tenetur
                        vel? Animi corporis cumque distinctio earum eius eos est fugiat, magnam molestias nihil
                        provident, reiciendis, rem sunt voluptas voluptatum!
                    </div>
                    <div>Corporis debitis dicta earum ipsum nobis qui quia! Aliquam asperiores consequuntur deserunt est
                        expedita fugit laudantium, numquam ratione reiciendis repellat suscipit ullam voluptatum.
                        Assumenda consequuntur ea incidunt laboriosam magnam, repudiandae?
                    </div>
                    <div>Aut corporis cupiditate eum ipsa laborum maiores porro qui quo recusandae reprehenderit! Amet
                        aspernatur dicta incidunt sit. Esse neque quae ullam. Amet at esse expedita iure magnam quasi
                        rerum sed?
                    </div>
                    <div>A amet corporis delectus esse harum illum laboriosam minima minus neque non odio, praesentium
                        repellat sit tempore, tenetur totam velit! At eveniet, incidunt. Animi consequuntur
                        exercitationem obcaecati rerum sit, soluta?
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>

</body>
</html>