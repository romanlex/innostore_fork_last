<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.Product" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Каталог</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="./includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="./includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page catalog">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                ${requestScope.error}
                            </p>
                        </div>
                    </c:if>

                    <div class="page__title">
                        <div class="page__title__left">
                            <h1>Каталог</h1>
                        </div>
                        <div class="page__title__right"></div>
                    </div>

                    <div class="catalog__products">
                        <c:if test="${not empty products}">
                            <ul>
                                <c:forEach items="${products}" var="product">
                                    <li data-product-id="${product.id}">
                                        <div class="product">
                                            <div class="product__image">
                                                <a href="/product/${product.id}">
                                                    <c:choose>
                                                        <c:when test="${empty product.image}">
                                                            <img src="/images/no-image.png" alt="">
                                                        </c:when>
                                                        <c:otherwise>
                                                            <img src="${product.image}" alt="">
                                                        </c:otherwise>
                                                    </c:choose>
                                                </a>
                                            </div>
                                            <div>
                                                <div class="product__title">
                                                    <a href="/product/${product.id}">${product.name}</a>
                                                </div>
                                                <c:if test="${not empty product.article}">
                                                    <div class="product__article">Арт. #${product.article}</div>
                                                </c:if>
                                                <div class="product__price">${product.price} ₽</div>
                                            </div>
                                        </div>
                                    </li>
                                </c:forEach>
                            </ul>
                        </c:if>
                        <c:if test="${empty products}">
                            <div class="secondary callout" style="margin-left: 10px; margin-right: 10px;">
                                К сожалению в каталоге не представлено ни одного продукта.
                            </div>
                        </c:if>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>

</body>
</html>