<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<section id="sidebar" class="sidebar ${isAdmin == true ? 'admin' : ''}">
    <div class="sidebar__head">
        <a href="/"><i class="fa fa-superpowers" aria-hidden="true"></i> InnoStore</a>
    </div>
    <div class="sidebar__content">
        <div class="profile">
            <div class="profile__photo">
                <c:choose>
                    <c:when test="${empty user.avatar}">
                        <img src="/images/no-avatar.png" alt="">
                    </c:when>
                    <c:otherwise>
                        <img src="${user.avatar}" alt="">
                    </c:otherwise>
                </c:choose>
            </div>
            <div class="profile__info">
                <span>Добро пожаловать</span><br>
                ${sessionScope.user.firstname}
            </div>
        </div>
        <div class="sidebar__menu">
            <ul>
                <security:authorize access="hasRole('ROLE_USER')">
                    <c:forEach items="${menu}" var="item">
                        <li class="${requestScope['javax.servlet.forward.servlet_path'] == item.key || (item.key == "/" && requestScope['javax.servlet.forward.servlet_path'] == null) ? 'active' : ''}">
                            <a href="${item.key}">${item.value}</a>
                        </li>
                    </c:forEach>
                </security:authorize>
                <security:authorize access="hasRole('ROLE_ADMIN') OR hasRole('ROLE_SUPERADMIN')">
                    <c:forEach items="${adminmenu}" var="item">
                        <li class="${requestScope['javax.servlet.forward.servlet_path'] == item.key || (item.key == "/" && requestScope['javax.servlet.forward.servlet_path'] == null) ? 'active' : ''}">
                            <a href="${item.key}">${item.value}</a>
                        </li>
                    </c:forEach>
                </security:authorize>
            </ul>
        </div>
    </div>
    <div class="sidebar__foot">
        <div class="status-order">
            <div class="status-order__title">
                Статус последнего заказа:
            </div>
            <div class="status-order__desc">
                <a href="/order/532">#532</a> ожидает подтверждения
            </div>
            <div class="status-order__time">
                Изменён: <span>08.03.2017</span>
            </div>
        </div>
    </div>
</section>