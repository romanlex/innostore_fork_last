<%@ page import="org.springframework.security.core.context.SecurityContextImpl" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page import="com.innostore.app.enums.User" %>

<%
    SecurityContextImpl sci = (SecurityContextImpl) request.getSession().getAttribute("SPRING_SECURITY_CONTEXT");
    User user = (User) sci.getAuthentication().getPrincipal();
    request.setAttribute("user", user);
    request.setAttribute("isAdmin", false);
%>

<security:authorize access="hasRole('ROLE_ADMIN') OR hasRole('ROLE_SUPERADMIN')">
    <% request.setAttribute("isAdmin", true); %>
</security:authorize>

<header class="header" id="header">
    <div class="topmenu">
        <ul class="topmenu__nav">
            <li class="left mobile">
                <button class="navbar-toggle" type="button" data-toggle="mobile-menu" data-target="#sidebar">
                    <span></span>
                </button>
            </li>
            <li>
                <a href="/profile" class="user-profile" data-toggle="dropdown">
                    <c:choose>
                        <c:when test="${empty user.avatar}">
                            <img src="/images/no-avatar.png" alt="">
                        </c:when>
                        <c:otherwise>
                            <img src="${user.avatar}" alt="">
                        </c:otherwise>
                    </c:choose>
                    ${user.firstname}
                    <span class="fa fa-angle-down"></span>
                </a>
                <div class="dropdown-menu">
                    <%@ include file="dropdown.profile.menu.jsp" %>
                </div>
            </li>
            <li>
                <a href="/basket" class="basket-button">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                    <span class="basket-button__count" id="basket-count">0</span>
                </a>
            </li>
        </ul>
    </div>
</header>
