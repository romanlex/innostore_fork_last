<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.Product" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Редактирование продукта</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="../includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="../includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page profile">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                    ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                    ${requestScope.error}
                            </p>
                        </div>
                    </c:if>

                    <a href="/catalog" class="button primary hollow">Назад к списку продуктов</a>

                    <form class="form-horizontal form-label-left" action="/product/edit/${product.id}" method="post" accept-charset="UTF-8">
                        <input type="hidden" name="id" value="${product.id}">
                        <div class="x_panel">
                            <div class="x_title">
                                <h3>Редактирование продука ${product.name} (#${product.article})</h3>
                            </div>
                            <div class="x_content">
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="article">Артикул</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="article" name="article" class="" value="${product.article}" >
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="name">Категория <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <select name="categoryId" id="categoryId">
                                        <c:forEach items="${categories}" var="category">
                                            <option value="${category.id}" <c:if test="${product.categoryId == category.id || param.categoryId == category.id}">selected</c:if>>${category.name}</option>
                                        </c:forEach>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="name">Наименование <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="name" name="name" required="required" class="" value="${product.name}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="longname">Полное наименование</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="longname" name="longname" class="" value="${product.longname}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="vendor">Производитель</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="vendor" name="vendor" class="" value="${product.vendor}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="price">Стоимость (в формате 10.00) <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="price" name="price" class="" pattern="[0-9]+(\.[0-9][0-9]?)?" required="required" value="${product.price}">
                                    </div>
                                </div>

                                <br>

                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="popular">Популярный</label>
                                    <div class="small-9 columns">
                                        <input type="checkbox" name="popular" id="popular" class="js-switch" <c:if test="${product.popular == 'Y'}">checked</c:if> />
                                    </div>
                                </div>

                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="description">Описание</label>
                                    <div class="small-9 columns">
                                        <textarea id="description" class="autosize" name="description">${product.description}</textarea>
                                    </div>
                                </div>

                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="button">Сохранить</button>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>
<c:if test="${(sessionScope.user.role == 'Admin' || sessionScope.user.role == 'SuperAdmin')}">
    <script src="/js/mgr.min.js"></script>
</c:if>
</body>
</html>