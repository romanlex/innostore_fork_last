<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.User" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Список заказов</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="../includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="../includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page users">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                ${requestScope.error}
                            </p>
                        </div>
                    </c:if>


                    <div class="x_panel">
                        <div class="x_title">
                            <h3>Заказы</h3>
                        </div>
                        <div class="x_content">
                            <c:if test="${not empty orders}">
                                <table class="table unstriped" cellpadding="0" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th width="40">#</th>
                                        <th width="80">Email</th>
                                        <th>Номер телефона</th>
                                        <th>Имя</th>
                                        <th>Пол</th>
                                        <th>Роль</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${orders}" var="order">
                                            <tr id="basket_product_1">
                                                <td>${order.id}</td>
                                                <td>
                                                    ${order.email}
                                                </td>
                                                <td>
                                                    ${order.phone}
                                                </td>
                                                <td>
                                                    ${order.firstname}
                                                </td>
                                                <td>
                                                    ${order.gender}
                                                </td>
                                                <td>
                                                    ${order.role}
                                                </td>
                                                <td>
                                                    <a href="order.html" class="hollow button tiny">Открыть</a>
                                                </td>
                                            </tr>
                                        </c:forEach>
                                     </tbody>
                                </table>
                            </c:if>
                            <c:if test="${empty orders}">
                                <div class="secondary callout">
                                    К сожалению еще нет ни одного зарегистрированного заказа.
                                </div>
                            </c:if>

                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>

</body>
</html>