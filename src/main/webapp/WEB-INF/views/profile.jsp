<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.User" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Профиль</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="./includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="./includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page profile">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                    ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                    ${requestScope.error}
                            </p>
                        </div>
                    </c:if>

                    <form class="form-horizontal form-label-left" action="/profile" method="post" accept-charset="UTF-8">
                        <div class="x_panel">
                            <div class="x_title">
                                <h3>Профиль</h3>
                            </div>
                            <div class="x_content">
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="firstname">Имя <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="firstname" name="firstname" required="required" class="" value="${user.firstname}" >
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="lastname">Фамилия <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="lastname" name="lastname" required="required" class="" value="${user.lastname}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="phone">Номер телефона <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="phone" name="phone" required="required" class="" value="${user.phone}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <div class="small-3 columns"></div>
                                    <div class="small-9 columns">
                                        <input type="radio" name="gender" value="1" id="man" <c:if test="${user.gender == '1'}">checked</c:if>>
                                        <label for="man">Мужской</label>

                                        <input type="radio" name="gender" value="0" id="woman" <c:if test="${user.gender == '0'}">checked</c:if>>
                                        <label for="woman">Женский</label>
                                    </div>
                                </div>

                                <hr>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="scope">Сфера работы</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="scope" name="scope" required="required" class="" value="${user.scope}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="position">Должность</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="position" name="position" required="required" class="" value="${user.position}">
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="country">Страна</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="country" name="country" class="" value="${user.country}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="region">Регион</label>
                                    <div class="small-9 columns">
                                        <input type="text" id="region" name="region" class="" value="${user.region}">
                                    </div>
                                </div>
                                <div class="form-group expanded row">
                                    <label class="small-3 columns" for="city">Город</label>
                                    <div class="small-9 columns">
                                        <div class="input-group">
                                            <input type="text" id="city" name="city" class="input-group-field" value="${user.city}">
                                            <span class="input-group-button button secondary">
                                                <button id="detectMyLocation" type="button"><i class="fa fa-location-arrow" aria-hidden="true"></i></button>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="button">Сохранить</button>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>
<script type="text/javascript" src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>
<script src="/js/profile.min.js"></script>

</body>
</html>