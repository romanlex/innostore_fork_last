<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Заказ</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="./includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="./includes/sidebar.jsp"%>
            <section class="content" id="content">
                <a href="myorders.html" class="back"><i class="fa fa-long-arrow-left" aria-hidden="true"></i>Назад к списку заказов</a>
                <div class="page order">
                    <div class="x_panel">
                        <div class="x_title">
                            <h3>Заказ №1242</h3>
                            <div class="right">
                                Дата заказа: 20.02.2017
                            </div>
                        </div>
                        <div class="x_content">
                            <table class="table unstriped" cellpadding="0" cellspacing="0">
                                <thead>
                                <tr>
                                    <th width="40">Артикул</th>
                                    <th>Продукт</th>
                                    <th>Стоимость</th>
                                    <th>Кол-во</th>
                                    <th>Сумма</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr id="basket_product_1">
                                    <td>
                                        #123
                                    </td>
                                    <td>
                                        Продукт 1
                                    </td>
                                    <td>500.00</td>
                                    <td>1</td>
                                    <td>
                                        500.00
                                    </td>
                                </tr>
                                <tr id="basket_product_2">
                                    <td>
                                        #123
                                    </td>
                                    <td>
                                        Продукт 1
                                    </td>
                                    <td>500.00</td>
                                    <td>1</td>
                                    <td>
                                        500.00
                                    </td>
                                </tr>
                                <tr id="basket_product_3">
                                    <td>
                                        #123
                                    </td>
                                    <td>
                                        Продукт 1
                                    </td>
                                    <td>500.00</td>
                                    <td>1</td>
                                    <td>
                                        500.00
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="amount">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th style="width:50%">Статус заказа:</th>
                                        <td><span class="label success">выполнен</span></td>
                                    </tr>
                                    <tr>
                                        <th>Сумма заказа:</th>
                                        <td>250.30 ₽</td>
                                    </tr>
                                    <tr>
                                        <th>Доставка:</th>
                                        <td>бесплатно</td>
                                    </tr>
                                    <tr>
                                        <th>Всего:</th>
                                        <td>265.24 ₽</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>

</body>
</html>