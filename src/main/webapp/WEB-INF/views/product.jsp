<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.innostore.app.enums.Product" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Продукт</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="./includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="./includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page product-page">
                    <c:if test="${requestScope.success != null}" >
                        <div class="success callout">
                            <p>
                                    ${requestScope.success}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error != null}" >
                        <div class="alert callout">
                            <p>
                                    ${requestScope.error}
                            </p>
                        </div>
                    </c:if>

                    <c:if test="${requestScope.error == null}" >
                        <div class="page__title">
                            <div class="page__title__left">
                                <h1>Карточка товара</h1>
                            </div>
                            <div class="page__title__right"></div>
                        </div>

                        <div class="x_panel">
                            <c:if test="${not empty product.name}" >
                                <div class="x_title">
                                    <h2>${product.name}</h2>
                                    <div class="clearfix"></div>
                                </div>
                            </c:if>
                            <div class="x_content expanded row">
                                <div class="columns small-5">
                                    <div class="product__photos">
                                        <div class="product__photo">
                                            <c:choose>
                                                <c:when test="${empty product.image}">
                                                    <img src="/images/no-image.png" alt="">
                                                </c:when>
                                                <c:otherwise>
                                                    <img src="${product.image}" alt="">
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns small-7">
                                    <form action="/rest/basket/add" method="post">
                                        <input type="hidden" name="id" value="1">
                                        <c:choose>
                                            <c:when test="${not empty product.longname}">
                                                <h3 class="product__title">${product.longname}</h3>
                                            </c:when>
                                            <c:otherwise>

                                            </c:otherwise>
                                        </c:choose>
                                        <div class="product__desc">
                                            <c:choose>
                                                <c:when test="${empty product.description}">
                                                    У товара пока нет описания. Мы стараемся над тем, чтобы оно появилось как можно скорее. Благодарим за понимание.
                                                </c:when>
                                                <c:otherwise>
                                                    ${product.description}
                                                </c:otherwise>
                                            </c:choose>
                                        </div>
                                        <div class="product__price">
                                            <h1 class="price">${product.price} ₽</h1>
                                            Доставка: 300 ₽ <span>(Бесплатно при сумме заказа от 3000 руб)</span>
                                        </div>
                                        <div class="product__buttons">
                                            <button type="button" class="hollow button secondary">Добавить в корзину</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>

</body>
</html>