<%@page pageEncoding="UTF-8" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,  initial-scale=1, user-scalable=yes">

    <title>InnoStore - Контакты</title>
    <meta name="description" content="">

    <link rel="stylesheet" href="/css/app.min.css">
    <link rel="stylesheet" href="/vendor/vendor.min.css">

    <link rel="shortcut icon" href="/favicon.ico" />
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">Вы используете <strong>устаревший</strong> браузер. Пожалуйста <a href="http://browsehappy.com/">обновите ваш браузер</a> для улучшения совместимости с приложением.</p>
<![endif]-->

<section class="layout">
    <div class="layout__wrapper">
        <%@include file="./includes/header.jsp"%>

        <div class="content-wrapper container">
            <%@include file="./includes/sidebar.jsp"%>
            <section class="content" id="content">
                <div class="page contacts">
                    <div class="page__title">
                        <div class="page__title__left">
                            <h1>Контакты</h1>
                        </div>
                        <div class="page__title__right"></div>
                    </div>

                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Обратная связь <small>Напишите нам если у вас есть вопросы</small></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <br>
                            <form class="form-horizontal form-label-left">
                                <div class="form-group row">
                                    <label class="small-3 columns" for="first-name">Фамилия <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="first-name" required="required" class="" >
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="small-3 columns" for="last-name">Имя <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <input type="text" id="last-name" name="last-name" required="required" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="small-3 columns">Текст вашего обращения <span class="required">*</span></label>
                                    <div class="small-9 columns">
                                        <textarea id="text" class="autosize" required="required" type="text"></textarea>
                                    </div>
                                </div>
                                <div class="ln_solid"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="button">Отправить</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</section>

<button id="page-scroll-up" data-toggle="scroll" data-target="up">
    <i class="fa fa-arrow-up" aria-hidden="true"></i>
</button>

<script src="/js/jquery.min.js"></script>
<script src="/vendor/vendor.min.js"></script>
<script src="/js/common.min.js"></script>

</body>
</html>